package tasks.week1;

import java.util.*;

public class MergingArrays {


    public static List<Integer> merge(List<Deque<Integer>> inputQueues, int[] arrayLengths) {
        // TODO: Implement the functionality here.

        List<Integer> merged = new ArrayList<>();

        // Taking first element from first stack
        Deque<Integer> minQueue = inputQueues.get(0);
        int minValue = minQueue.getFirst();

        List<Deque<Integer>> queues = new ArrayList<>(inputQueues);

        while (!queues.isEmpty()) {
            for (int i = 0; i < queues.size(); i++) {
                Deque<Integer> currentQueue = queues.get(i);

                if (!currentQueue.isEmpty()) {
                    Integer firstElementOfCurrentQueue = currentQueue.getFirst();

                    if (firstElementOfCurrentQueue <= minValue) {
                        minValue = firstElementOfCurrentQueue;
                        minQueue = currentQueue;
                    }

                    if (i + 1 == queues.size()) {
                        merged.add(minQueue.pollFirst());
                        minValue = Integer.MAX_VALUE;
                    }
                } else {
                    queues.remove(currentQueue);
                }
            }
        }

        return merged;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numArrays = Integer.parseInt(scanner.nextLine());
        int[] arrayLengths = Arrays.stream(scanner.nextLine().split(" ")).mapToInt(Integer::parseInt).toArray();
        List<Deque<Integer>> arrays = new ArrayList<>();
        for (int i = 0; i < numArrays; ++i) {
            int[] array = Arrays.stream(scanner.nextLine().split(" ")).mapToInt(Integer::parseInt).toArray();
            Deque<Integer> stack = new ArrayDeque<>();
            for (int el : array) stack.add(el);
            arrays.add(stack);
        }
        scanner.close();

        // TODO: Implement the merge() function.
        List<Integer> merged = merge(arrays, arrayLengths);

        StringBuilder sb = new StringBuilder();
        for (int s : merged) {
            sb.append(s);
            sb.append(" ");
        }
        System.out.println(sb.toString());
    }

}
