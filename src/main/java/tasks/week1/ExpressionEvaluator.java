package tasks.week1;

import java.io.IOException;
import java.util.Scanner;

public class ExpressionEvaluator {

    int position;
    int currentCharacter;
    String expression;

    public ExpressionEvaluator(String expression) {
        this.expression = expression;
        this.position = -1;
    }


    double evaluate() throws IOException {
        next();
        return evaluateExpression();
    }

    void next() {
        currentCharacter = ++position < expression.length() ? expression.charAt(position) : -1;
    }

    double evaluateExpression() {
        double result = evaluateTerm();
        while (true) {
            if (shouldApply('+')) {
                result += evaluateTerm();
            } else if (shouldApply('-')) {
                result -= evaluateTerm();
            } else if (shouldApply('a')) {
                next();
                next();
                result = Math.abs(evaluateTerm());
            } else {
                return result;
            }
        }
    }

    double evaluateTerm() {
        double result = evaluateFactor();
        while (true) {
            if (shouldApply('*')) {
                result *= evaluateFactor();
            } else if (shouldApply('/')) {
                double factor = evaluateFactor();
                if (factor == 0) {
                    throw new NumberFormatException();
                }
                result /= factor;
            } else if (position != expression.length()-1) {
                next();
            } else {
                return result;
            }
        }
    }


    double evaluateFactor() {
        double result = 0.0;
        if (shouldApply('(')) {
            result = evaluateExpression();
            shouldApply(')');
        } else if (currentCharacter >= '0' && currentCharacter <= '9') {
            next();
            result = Double.parseDouble(expression.substring(position - 1, position));

            if (shouldApply('.')) {
                double factor = 0.0;
                if (currentCharacter >= '0' && currentCharacter <= '9') {
                    next();
                    factor = Double.parseDouble(expression.substring(position - 1, position));
                }
                result = result + factor * 0.1;
            } else if (shouldApply('^')) {
                result = Math.pow(result, evaluateFactor());
            }
        }
        return result;
    }

    boolean shouldApply(char c) {
        if (currentCharacter == c) {
            next();
            return true;
        }
        return false;
    }


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String expression = scanner.nextLine();
        ExpressionEvaluator expressionEvaluator = new ExpressionEvaluator(expression);
        try {
            double evaluate = expressionEvaluator.evaluate();
            System.out.printf("%.2f", evaluate);
        } catch (Exception e) {
            System.out.println("Invalid mathematical expression.");
        }
        scanner.close();
    }
}
