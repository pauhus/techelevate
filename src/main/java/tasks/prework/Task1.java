package tasks.prework;

import java.util.*;
import java.util.stream.Collectors;

public class Task1 {

    public static void main(String[] args) {
        final Scanner sc = new Scanner(System.in);
        final int n = sc.nextInt();
        Set<Integer> row = new TreeSet<>(Comparator.reverseOrder());

        for (int i = 1; i <= n * n; i++) {
            boolean isEvenRow = (i / n) % 2 == 0;

            if (isEvenRow) {
                System.out.print(i);
                System.out.print(" ");
            } else {
                row.add(i);
            }

            if (i % n == 0) {
                System.out.println(row.stream().map(String::valueOf).collect(Collectors.joining(" ")));
                System.out.println();
                row = new TreeSet<>(Comparator.reverseOrder());
            }
        }
    }


    /**
     * N = 3
     *
     * 1 2 3
     * 6 5 4
     * 7 8 9
     *
     */


    /**
     * N = 2
     *
     * 1 2
     * 4 3
     *
     */


    /**
     * N = 5
     *
     * 1  2  3  4  5
     * 6  7  8  9  10
     * 11 12 13 14 15
     * 16 17 18 19 20
     * 21 22 23 24 25
     *
     */
}
