package tasks.week3;

import java.util.Arrays;
import java.util.Scanner;

public class CheckArrayOrder {

    public static int checkArrayOrder(int[] inputArray) {
        if (inputArray == null || inputArray.length < 2) {
            return 0;
        }

        if (inputArray[0] <= inputArray[1]) {
            return checkIfSortedAscending(inputArray);
        } else {
            return checkIfSortedDescending(inputArray);
        }
    }

    private static int checkIfSortedDescending(int[] inputArray) {
        for (int i = 0; i < inputArray.length - 1; i++) {
            if (inputArray[i] < inputArray[i + 1]) {
                return 0;
            }
        }
        return -1;
    }

    private static int checkIfSortedAscending(int[] inputArray) {
        for (int i = 0; i < inputArray.length - 1; i++) {
            if (inputArray[i] > inputArray[i + 1]) {
                return 0;
            }
        }
        return 1;
    }

    private static int oldWayOfChecking(int[] inputArray) {
        //copy of array
        int[] sortedArray = Arrays.copyOf(inputArray, inputArray.length);

        Arrays.sort(sortedArray);

        //check if array is sorted
        if (Arrays.equals(inputArray, sortedArray)) {
            return 1;
        }


        //reverse sorted array
        for (int i = 0; i < sortedArray.length / 2; i++) {
            int temp = sortedArray[i];
            sortedArray[i] = sortedArray[sortedArray.length - i - 1];
            sortedArray[sortedArray.length - i - 1] = temp;
        }

        //check if array is sorted
        if (Arrays.equals(inputArray, sortedArray)) {
            return -1;
        }

        //array not sorted
        return 0;
    }

    public static void main(String[] args) {
        final int[] inputNumbers = readInputArray();
        final int sortedOrder = checkArrayOrder(inputNumbers);
        System.out.println(sortedOrder);
    }

    private static int[] readInputArray() {
        final Scanner scanner = new Scanner(System.in);
        final int numberOfElements = scanner.nextInt();
        int[] inputArray = null;

        if (numberOfElements > 0) {
            inputArray = new int[numberOfElements];
        }
        int i = 0;
        while (i < numberOfElements) {
            inputArray[i++] = scanner.nextInt();
        }
        scanner.close();
        return inputArray;
    }
}
