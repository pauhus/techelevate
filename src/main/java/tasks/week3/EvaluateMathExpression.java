package tasks.week3;

import java.util.Scanner;

public class EvaluateMathExpression {

    public static double evaluateMathExpression(String input) {
        final String[] inputArray = parseInputString(input);
        // s1 is first number.
        final String leftValue = inputArray[0];
        // operator is math operator (e.g. '+').
        final String operator = inputArray[1];
        // rightValue is second number.
        final String rightValue = inputArray[2];

        // first number
        final double valueLeft = parseValue(leftValue);
        // second number
        final double valueRight = parseValue(rightValue);

        final Operation operation = evaluateOperation(operator);

        return operation.apply(valueLeft, valueRight);
    }

    private static Operation evaluateOperation(String operator) {
        switch (operator) {
            case "+":
                return new AddOperation();
            case "-":
                return new SubtractOperation();
            case "/":
                return new DivideOperation();
            case "*":
                return new MultiplyOperation();
            default:
                return new NoOperation();
        }
    }

    private static double parseValue(String stringValue) {
        double value = 0.0;
        for (int i = 0; i < stringValue.length(); i++) {
            char c = stringValue.charAt(i);
            if (c < '0' || c > '9') {
                throw new NumberFormatException("Can't convert character to digit: " + c);
            }
            int digit = c - '0';
            value += digit * Math.pow(10, stringValue.length() - i - 1);
        }
        return value;
    }

    private static String[] parseInputString(String input) {
        if (input == null) {
            throw new IllegalArgumentException("Must not be null");
        }
        final String[] inputArray = input.split(" ");

        if (inputArray.length != 3) {
            throw new IllegalArgumentException("Must have 3 tokens separated by spaces");
        }
        return inputArray;
    }

    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        String s = cin.nextLine();
        cin.close();

        try {
            double result = evaluateMathExpression(s);
            System.out.print(result);
        } catch (NumberFormatException e) {
            System.out.print("NumberFormatException " + e.getMessage());
        } catch (IllegalArgumentException e) {
            System.out.print("IllegalArgumentException " + e.getMessage());
        }
    }

    interface Operation {

        double apply(double valueLeft, double valueRight);
    }

    public static class AddOperation implements Operation {

        @Override
        public double apply(double valueLeft, double valueRight) {
            return valueLeft + valueRight;
        }
    }

    public static class SubtractOperation implements Operation {

        @Override
        public double apply(double valueLeft, double valueRight) {
            return valueLeft - valueRight;
        }
    }

    public static class MultiplyOperation implements Operation {

        @Override
        public double apply(double valueLeft, double valueRight) {
            return valueLeft * valueRight;
        }
    }

    public static class DivideOperation implements Operation {

        @Override
        public double apply(double valueLeft, double valueRight) {
            if (valueRight == 0) {
                throw new IllegalArgumentException("Can't divide by 0");
            }
            return valueLeft / valueRight;
        }
    }

    public static class NoOperation implements Operation {

        @Override
        public double apply(double valueLeft, double valueRight) {
            throw new NumberFormatException("Can't convert character to operator: +, -, /, *");
        }
    }

}
