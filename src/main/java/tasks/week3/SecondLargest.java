package tasks.week3;

import java.util.Scanner;

public class SecondLargest {

    public static int secondLargest(int[] inputList) {
        if (inputListIsIncorrect(inputList)) {
            return -1;
        }

        int max = inputList[0];

        int secondMax = max;

        for (int current : inputList) {
            if (current > max) {
                secondMax = max;
                max = current;
            } else {
                if (current > secondMax && current != max) {
                    secondMax = current;
                } else {
                    if (secondMax == max && current < secondMax) {
                        secondMax = current;
                    }
                }
            }
        }

        if (secondMax == max) {
            return -1;
        }

        return secondMax;
    }

    private static boolean inputListIsIncorrect(int[] inputList) {
        return inputList == null || inputList.length < 2;
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int numberOfElements = input.nextInt();
        int[] numbers = {};

        if (numberOfElements > 0) {
            numbers = new int[numberOfElements];
        }

        int i = 0;

        while (i < numberOfElements) {
            numbers[i++] = input.nextInt();
        }

        input.close();

        int result = secondLargest(numbers);
        System.out.println(result);
    }
}
