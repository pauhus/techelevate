package tasks.week2;

import java.util.*;

public class DictionaryTree {


    public static void main(String[] args) {

        final Scanner sc = new Scanner(System.in);

        final int n = sc.nextInt();
        List<String> dict = new ArrayList<>(n);
        sc.nextLine();
        for (int i = 0; i < n; ++i) {
            dict.add(sc.nextLine());
        }

        final int q = sc.nextInt();
        List<String> queries = new ArrayList<>(q);
        sc.nextLine();
        for (int i = 0; i < q; ++i) {
            queries.add(sc.nextLine());
        }

        solve(dict, queries);
    }


    static void solve(List<String> dict, List<String> queries) {
        Node dictionaryRoot = initDictionary(dict);


        for (String query : queries) {
            Node node = locateNode(dictionaryRoot, query);
            Deque<Node> deque = new ArrayDeque<>();
            if (node != null) {
                deque.add(node);
            }

            List<String> result = new ArrayList<>();

            while (!deque.isEmpty() && result.size() <= 10) {
                Node last = deque.pollLast();
                if (last.word != null) {
                    result.add(last.word);
                }
                deque.addAll(last.dictionary.values());
            }

            System.out.println(String.join(" ", result));

        }

    }

    private static Node locateNode(Node dictionaryRoot, String query) {
        Node root = dictionaryRoot;
        int wrongLetter = 0;

        char[] chars = query.toCharArray();

        for (int i = 0; i < chars.length; i++) {
            String currentChar = String.valueOf(chars[i]);
            Map<String, Node> dictionary = root.dictionary;
            Node result = dictionary.get(currentChar);

            if (result != null) {
                root = result;
            } else {
                wrongLetter = wrongLetter + 1;

                if (wrongLetter == 1 && i <= chars.length - 1) {
                    String nextChar = String.valueOf(chars[i + 1]);

                    Optional<Node> any = dictionary.values().stream().map(node -> {

                                Node node1 = node.dictionary.get(nextChar);
                                if (node1 != null) {
                                    return node;
                                } else {
                                    return null;
                                }
                            }

                    ).filter(Objects::nonNull).findAny();

                    if (any.isPresent()) {
                        root = any.get();
                    } else {
                        return null;
                    }
                } else {
                    return null;
                }
            }
        }
        return root;
    }

    private static Node initDictionary(List<String> dict) {
        Node root = new Node();
        for (String word : dict) {
            insertWordToDictionary(root, word);
        }
        return root;
    }

    private static void insertWordToDictionary(Node root, String word) {
        char[] chars = word.toCharArray();
        Node currentNode = root;
        for (char c : chars) {
            Map<String, Node> dictionary = currentNode.dictionary;
            if (dictionary.containsKey(String.valueOf(c))) {
                currentNode = dictionary.get(String.valueOf(c));
            } else {
                Node newNode = new Node();
                dictionary.put(String.valueOf(c), newNode);
                currentNode = newNode;
            }
        }
        currentNode.word = word;
    }

    public static class Node {
        Map<String, Node> dictionary = new HashMap<>();
        String word;

        @Override
        public String toString() {
            return "Node{" +
                    "dictionary=" + dictionary +
                    ", word='" + word + '\'' +
                    '}';
        }
    }

}
