package tasks.week2;

import java.util.*;

public class Dictionary {

    public static void main(String[] args) {

        final Scanner sc = new Scanner(System.in);

        final int n = sc.nextInt();
        List<String> dict = new ArrayList<>(n);
        sc.nextLine();
        for (int i = 0; i < n; ++i) {
            dict.add(sc.nextLine());
        }

        final int q = sc.nextInt();
        List<String> queries = new ArrayList<>(q);
        sc.nextLine();
        for (int i = 0; i < q; ++i) {
            queries.add(sc.nextLine());
        }

        solve(dict, queries);
    }


    static void solve(List<String> dict, List<String> queries) {
        dict.sort(String::compareToIgnoreCase);
        queries.sort(String::compareToIgnoreCase);

        for (String query : queries) {
            Set<String> matches = findMatches(dict, query);
            if (matches.isEmpty()) {
                System.out.println("<no matches>");
            } else {
                String outputString = String.join(" ", matches);
                System.out.println(outputString);
            }
        }
    }

    private static Set<String> findMatches(List<String> dict, String query) {
        Set<String> matchesList = new HashSet<>();

        for (String word : dict) {
            char[] wordChars = word.toCharArray();
            char[] queryChars = query.toCharArray();
            int typo = 0;


            for (int i = 0; i < queryChars.length; i++) {
                if (i < wordChars.length) {
                    if (wordChars[i] != queryChars[i]) {
                        typo = typo + 1;
                    }
                    if (typo > 1) {
                        break;
                    }
                }
            }

            if (typo <= 1 && queryChars.length <= wordChars.length) {
                matchesList.add(word);
            }

        }

        return matchesList;
    }
}
