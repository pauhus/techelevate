package tasks.week3;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EvaluateMathExpressionTest {


    @Test
    public void testTask2() {
        // Given
        String input = "8 + 10";

        // When
        double result = EvaluateMathExpression.evaluateMathExpression(input);

        // Then
        assertEquals(18.0, result);
    }

}