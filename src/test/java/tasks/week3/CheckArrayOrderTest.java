package tasks.week3;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CheckArrayOrderTest {


    @Test
    public void testTask3() {
        // Given
        int[] array = {
                1,
                2,
                3};
        // When
        int result = CheckArrayOrder.checkArrayOrder(array);

        // Then
        assertEquals(1, result);
    }

    @Test
    public void testTask3Descending() {
        // Given
        int[] array = {
                3,
                2,
                1};
        // When
        int result = CheckArrayOrder.checkArrayOrder(array);

        // Then
        assertEquals(-1, result);
    }


    @Test
    public void testTask3NullInput() {
        // Given
        int[] array = null;
        // When
        int result = CheckArrayOrder.checkArrayOrder(array);

        // Then
        assertEquals(0, result);
    }


    @Test
    public void testTask3Equals() {
        // Given
        int[] array = {
                1,
                1,
                1};
        // When
        int result = CheckArrayOrder.checkArrayOrder(array);

        // Then
        assertEquals(1, result);
    }
}
