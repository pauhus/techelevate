package tasks.week3;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SecondLargestTest {


    @Test
    public void shouldReturnSecondMax() {
        // Given
        int[] inputList = {1, 1, 1, 1};

        // When
        int result = SecondLargest.secondLargest(inputList);

        // Then
        assertEquals(-1, result);
    }


    @Test
    public void shouldReturnSecondMaxNegativeNumbers() {
        // Given
        int[] inputList = {
                -2,
                -2,
                -3,
                -4,
                -3};

        // When
        int result = SecondLargest.secondLargest(inputList);

        // Then
        assertEquals(-3, result);
    }


    @Test
    public void shouldReturnSecondMaxDescription2() {
        // Given
        int[] inputList = {
                3,
                10,
                5,
                10};

        // When
        int result = SecondLargest.secondLargest(inputList);

        // Then
        assertEquals(5, result);
    }


}