package tasks.week2;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

class DictionaryTreeTest {

    @Test
    public void test() {
        // Given
        List<String> dictionary = Arrays.asList("tech",

                "computer",

                "technology",

                "elevate",

                "compute",

                "elevator",

                "company");
        List<String> query = Arrays.asList(
                "tevh",

                "new",

                "techn",

                "compa"
        );

        // When
        DictionaryTree.solve(dictionary, query);

        // Then
    }
}