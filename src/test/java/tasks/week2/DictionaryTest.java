package tasks.week2;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

class DictionaryTest {


    @Test
    public void test() {
        // Given
        List<String> dictionary = Arrays.asList("tech",

                "computer",

                "technology",

                "elevate",

                "compute",

                "elevator",

                "company");
        List<String> query = Arrays.asList("tevh",

                "new",

                "techn",

                "compa");

        // When
        Dictionary.solve(dictionary, query);

        // Then
    }

}
