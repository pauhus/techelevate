package tasks.week1;

import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class MergingArraysTest {


    @Test
    public void mergeArrays() {
        // Given
        int[] arrayLenghts = {2, 3, 4};
        List<Deque<Integer>> lists = new ArrayList<>();

        Deque<Integer> stack1 = new ArrayDeque<>();
        stack1.add(1);
        stack1.add(3);
        lists.add(stack1);

        Deque<Integer> stack2 = new ArrayDeque<>();
        stack2.add(2);
        stack2.add(4);
        stack2.add(5);
        lists.add(stack2);

        Deque<Integer> stack3 = new ArrayDeque<>();
        stack3.add(2);
        stack3.add(3);
        stack3.add(3);
        stack3.add(4);
        lists.add(stack3);


        List<Integer> expectedMerged = Stream.of(1, 2, 2, 3, 3, 3, 4, 4, 5).collect(Collectors.toList());

        // When
        List<Integer> merged = MergingArrays.merge(lists, arrayLenghts);

        // Then
        assertArrayEquals(expectedMerged.toArray(), merged.toArray());

    }
}