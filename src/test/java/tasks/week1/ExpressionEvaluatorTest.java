package tasks.week1;

import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class ExpressionEvaluatorTest {


    @Test
    public void testExpression() throws IOException {
        // given
        String expression = "-3^4";

        // when
        double result = new ExpressionEvaluator(expression).evaluate();

        // then
        assertEquals(-81.00, result);
    }

    @Test
    public void testExpressionAbs() throws IOException {
        // given
        String expression = "abs(2.2-(3*2))";

        // when
        double result = new ExpressionEvaluator(expression).evaluate();

        // then
        assertEquals(3.8, result);
    }

    @Test
    public void testExpression3() throws IOException {
        // given
        String expression = "1+2*3^4";

        // when
        double result = new ExpressionEvaluator(expression).evaluate();

        // then
        assertEquals(163.0, result);
    }

//    @Test
//    public void testExpressionZero() throws IOException {
//        // given
//        String expression = "1/0";
//
//        // when
//        double result = new ExpressionEvaluator(expression).evaluate();
//
//        // then
////        assertEquals(163.0, result);
//    }

    @Test
    public void testExpression5() throws IOException {
        // given
        String expression = "12345677*-(-(-(((98765431.)))))";

        // when
        double result = new ExpressionEvaluator(expression).evaluate();

        // then
        assertEquals(-1219326109891787.00, result);
    }

    @Test
    public void testExpression6() throws IOException {
        // given
        String expression = "(-3)^4";

        // when
        double result = new ExpressionEvaluator(expression).evaluate();

        // then
        assertEquals(0, result);
    }


}

